package Renato_Marcelo;
import robocode.*;
import java.awt.Color;

 
public class Sliphy_SP extends AdvancedRobot
{

	public void run() {
	
		boolean angulo=true;

		 //Cores do Robo
		
		setBodyColor(Color.red); //Corpo
		setGunColor(Color.black); //Arma
		setRadarColor(Color.red); //Radar
		setScanColor(Color.yellow); //Varredura
		setBulletColor(Color.red); //Bala
		
		// Movimentação do Robo
		while(true) {
			
		if(angulo==true)
		{
		for(int voltasD=0;voltasD<4;voltasD++)
		
		{

		setAhead(100);
		setTurnRight(90);
		setTurnGunRight(90);
		execute();
		}
		
			for(int voltasD=0;voltasD<4;voltasD++)
		
		{

				setAhead(100);
				setTurnLeft(90);
				setTurnGunLeft(180);
				back(50);
				execute();
		  }
		  angulo=false;
		}
		else{
			for(int voltasD=0;voltasD<4;voltasD++)
		
		{

		setAhead(100);
		setTurnRight(90);
		setTurnGunRight(90);
		execute();
		}
		
			for(int voltasD=0;voltasD<3;voltasD++)
		
		{

				setAhead(100);
				setTurnLeft(90);
				setTurnGunLeft(180);			
				execute();
		  }
		  angulo=true;
		}	
	}
}

	public void onScannedRobot(ScannedRobotEvent e) {	
		fire(2);
	}


	public void onHitByBullet(HitByBulletEvent e) {	
		back(10);
	}
	
	public void onHitWall(HitWallEvent e) {	
		back(20);
	}	
}
